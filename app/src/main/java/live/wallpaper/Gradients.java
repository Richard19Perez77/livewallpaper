package live.wallpaper;

import android.graphics.Canvas;
import android.view.MotionEvent;

public class Gradients {

    final int TYPE_GRADIENTS = 2;
    final int MAX_GRADIENTS = 5;

    Gradient[] gradients = new Gradient[MAX_GRADIENTS];
    Gradient tempGradient;
    boolean[] exists = new boolean[MAX_GRADIENTS];
    int radius;

    public void actionDown(MotionEvent event) {
        int downX = (int) event.getX();
        int downY = (int) event.getY();

        boolean found = false;
        for (int i = 0; i < exists.length; i++) {
            if (!exists[i]) {
                exists[i] = true;
                tempGradient = new Gradient(radius);
                tempGradient.createGradient(downX, downY, i % TYPE_GRADIENTS);
                gradients[i] = tempGradient;
                found = true;
                break;
            }
        }

        if (!found) {
            for (int i = 0; i < exists.length; i++) {
                exists[i] = false;
                gradients[i] = null;
            }
        }
    }

    public void updatePhysics() {
        for (int i = 0; i < exists.length; i++) {
            if (exists[i]) {
                gradients[i].updatePhysics();
            }
        }
    }

    public void draw(Canvas canvas) {
        for (int i = 0; i < exists.length; i++) {
            if (exists[i]) {
                gradients[i].draw(canvas);
            }
        }
    }

    public void onSurfaceChanged(int width, float height) {
        if (height > width)
            radius = (int) (height / 2f);
        else
            radius = (int) (width / 2f);
    }
}