package live.wallpaper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;

public class Gradient {

    final float WIDTH_RATIO = .5f;
    final float SPEED_RATIO = .075f;

    int x, y;
    int color, transparent = Color.TRANSPARENT;
    float gradientCount, gradientIndex, gradientRadius, gradientStep;
    boolean incR = true;

    RadialGradient[] gradients;
    RadialGradient currGradient;
    Paint paint = new Paint();

    public Gradient(int r) {
        gradientRadius = ((float) r) * WIDTH_RATIO;
        gradientCount = gradientRadius * SPEED_RATIO;
        gradientStep = gradientRadius / gradientCount;
        gradients = new RadialGradient[(int) gradientCount];
    }

    public void createGradient(float downX, float downY, int colorIndex) {
        x = (int) downX;
        y = (int) downY;
        setDefaultColor(colorIndex);
        float stepAmount = 1f;
        for (int i = 0; i < (int) gradientCount; i++) {
            stepAmount += gradientStep;
            currGradient = new RadialGradient(x, y, (int) stepAmount, color,
                    transparent, android.graphics.Shader.TileMode.CLAMP);
            gradients[i] = currGradient;
        }
    }

    public void updatePhysics() {
        currGradient = gradients[(int) gradientIndex];
        paint.setShader(currGradient);

        if (gradientIndex == gradientCount - 1)
            incR = false;
        else if (gradientIndex == 0)
            incR = true;

        if (incR) {
            gradientIndex++;
            if (gradientIndex > gradientCount - 1)
                gradientIndex = gradientCount - 1;
        } else {
            gradientIndex--;
            if (gradientIndex < 0)
                gradientIndex = 0;
        }
    }

    public void draw(Canvas canvas) {
        if (currGradient != null)
            canvas.drawCircle(x, y, gradientRadius, paint);
    }

    public void setPaintViolet() {
        int violet = Color.parseColor("#F60080");
        color = violet;
    }

    public void setPaintBlue() {
        color = Color.BLUE;
    }

    public void setDefaultColor(int colorIndex) {
        switch (colorIndex) {
            case 0:
                setPaintViolet();
                break;
            case 1:
                setPaintBlue();
                break;
        }
    }
}