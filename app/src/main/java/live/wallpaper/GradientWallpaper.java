package live.wallpaper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

/*
 * This animated wallpaper draws a rotating wireframe cube.
 */
public class GradientWallpaper extends WallpaperService {

    private final Handler mHandler = new Handler();
    private static final String TAG = "livewallpaper";

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate GradientWallpaper");
        super.onCreate();
    }

    @Override
    public Engine onCreateEngine() {
        Log.d(TAG, "onCreateEngine GradientWallpaper");
        return new GradientEngine();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy GradientWallpaper");
        super.onDestroy();
    }

    public class GradientEngine extends Engine {

        ScaleBitmap scaleBitmap;
        private int WALLPAPER = 0;
        private boolean isVisible;
        Gradients gradients = new Gradients();
        Bitmap wallpaper;
        int x = 0, y = 0, screenW, screenH;
        boolean isWallpaperLoaded;

        private final Runnable drawFrame = new Runnable() {
            public void run() {
                drawFrame();
            }
        };

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            Log.d(TAG, "onCreate");
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
            scaleBitmap = new ScaleBitmap();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            Log.d(TAG, "onVisibilityChanged " + visible);
            isVisible = visible;
            if (visible) {
                drawFrame();
            } else {
                mHandler.removeCallbacks(drawFrame);
            }
        }

        @Override
        public void onDestroy() {
            Log.d(TAG, "onDestroy");
            super.onDestroy();
            mHandler.removeCallbacks(drawFrame);
        }

        /*
         * Store the position of the touch event so we can use it for drawing later
         */
        @Override
        public void onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case (MotionEvent.ACTION_DOWN):
                    gradients.actionDown(event);
                    break;
            }
            super.onTouchEvent(event);
        }

        /*
         * Draw one frame of the animation. This method gets called repeatedly
         * by posting a delayed Runnable. You can do any drawing you want in
         * here. This example draws a wireframe cube.
         */
        void drawFrame() {
            Log.d(TAG, "drawFrame");
            final SurfaceHolder holder = getSurfaceHolder();

            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    updatePhysics();
                    draw(c);
                }
            } finally {
                if (c != null) holder.unlockCanvasAndPost(c);
            }

            // Reschedule the next redraw
            mHandler.removeCallbacks(drawFrame);
            if (isVisible) {
                mHandler.postDelayed(drawFrame, 1000 / 25);
            }
        }

        public void draw(Canvas c) {
            Log.d(TAG, "draw");
            c.drawColor(Color.BLACK);
            c.drawBitmap(wallpaper, x, y, null);
            gradients.draw(c);
        }

        public void updatePhysics() {
            Log.d(TAG, "updatePhysics");
            gradients.updatePhysics();
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "onSurfaceCreated");
            super.onSurfaceCreated(holder);
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            Log.d(TAG, "onSurfaceChanged");
            Log.d(TAG, "width = " + width);
            Log.d(TAG, "height = " + height);
            screenW = width;
            screenH = height;
            gradients.onSurfaceChanged(width, height);
            wallpaper = scaleBitmap.loadScaledBitmap(getResources(), WALLPAPER, screenW, screenH, this);
            drawFrame();
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                                     float xStep, float yStep, int xPixels, int yPixels) {
            Log.d(TAG, "onOffsetsChanged");
            Log.d(TAG, "xOffset = " + xOffset);
            Log.d(TAG, "yOffset = " + yOffset);
            Log.d(TAG, "xStep = " + xStep);
            Log.d(TAG, "yStep = " + yStep);
            Log.d(TAG, "xPixels = " + xPixels);
            Log.d(TAG, "yPixels = " + yPixels);
            if (isWallpaperLoaded == false) {
                int fullWidth = (int) (screenW / xStep);
                wallpaper = scaleBitmap.loadScaledBitmap(getResources(), WALLPAPER, fullWidth, screenH, this);
                isWallpaperLoaded = true;
            }
            Log.d(TAG, "background h = " + wallpaper.getHeight());
            Log.d(TAG, "background w = " + wallpaper.getWidth());
            x = xPixels;
            Log.d(TAG, "x = " + x);
            drawFrame();
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onSurfaceDestroyed");
            super.onSurfaceDestroyed(holder);
            isVisible = false;
            mHandler.removeCallbacks(drawFrame);
        }
    }
}
